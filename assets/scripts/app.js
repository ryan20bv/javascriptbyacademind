

const defaultResult = 0;
let currentResult = defaultResult;
let logEntries = [];


function getUserNumberInput(){
     return parseFloat(userInput.value);
}

function createAndWriteOutput (operator, previousResult, enteredNumber){
     const description = `${previousResult} ${operator} ${enteredNumber}`
     outputResult(currentResult,  description);
}


function add(){
     const enteredNumber = getUserNumberInput();
     if( userInput.value == ""){
          p.style.visibility = "visible";
     }
     else {
          
               const previousResult = currentResult;
               logEntries.push(currentResult, "+", enteredNumber);
               currentResult += enteredNumber;
               createAndWriteOutput('+', previousResult, enteredNumber);
               userInput.value = "";
               p.style.visibility = "hidden";
               
               console.log(logEntries);
     }
     
}
function subtract(){
     const enteredNumber = getUserNumberInput();
     if( userInput.value == ""){
          p.style.visibility = "visible";
     }
     else {
          const previousResult = currentResult;
          currentResult -= enteredNumber;
          createAndWriteOutput('-', previousResult, enteredNumber);
          userInput.value = "";
          p.style.visibility = "hidden";
     }
     
}
function multiply(){
     const enteredNumber = getUserNumberInput();
     if( userInput.value == ""){
          p.style.visibility = "visible";
     }
     else {
          const previousResult = currentResult;
          currentResult *= enteredNumber;
          createAndWriteOutput('*', previousResult, enteredNumber);
          userInput.value = "";
          p.style.visibility = "hidden";
     }
     
}

function divide(){
     const enteredNumber = getUserNumberInput();
     if( userInput.value == ""){
          p.style.visibility = "visible";
     }
     else {
          const previousResult = currentResult;
          currentResult /= enteredNumber;
          createAndWriteOutput('/', previousResult, enteredNumber);
          userInput.value = "";
          p.style.visibility = "hidden";
     }
     
}


addBtn.addEventListener('click', add);
subtractBtn.addEventListener('click', subtract);
multiplyBtn.addEventListener('click', multiply);
divideBtn.addEventListener('click', divide);
